import os

from conans import ConanFile, tools


class FastFloatConan(ConanFile):
    name = "fast_float"
    license = "Apache-2.0"
    author = "toge.mail@gmail.com"
    homepage = "https://github.com/fastfloat/fast_float/"
    url = "https://bitbucket.org/toge/conan-fast_float/src"
    description = "Fast and exact implementation of the C++ from_chars functions for float and double types "
    topics = ("from_chars", "cpp17", "cpp11")
    no_copy_source = True
    # No settings/options are necessary, this is header only

    def source(self):
        '''retrieval of the source code here. Remember you can also put the code
        in the folder and use exports instead of retrieving it with this
        source() method
        '''
        tools.get(**self.conan_data["sources"][self.version])
        os.rename("fast_float-{}".format(self.version), "fast_float")

    def package(self):
        self.copy("*.h", dst="include", src="fast_float/include")

