#include <iostream>
#include "fast_float/fast_float.h"

int main() {
    char input[] = "3.1416 aaa";

    double result;
    auto answer = fast_float::from_chars(input, input + sizeof(input), result);
    if (answer.ec != std::errc()) {
        std::cerr << "parssing failure" << std::endl;
        return 1;
    }

    std::cout << "parsed the number " << result << std::endl;

    return 0;
}
